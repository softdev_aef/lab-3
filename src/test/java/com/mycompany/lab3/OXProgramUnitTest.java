/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author fkais
 */
public class OXProgramUnitTest {
    
    public OXProgramUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testCheckWinFalse() {
        char turn = 'X';
        char[][] board = {{'X','O','-'},{'X','X','O'},{'-','-','-'}};
        assertEquals(false,OXProgram.Win(board, turn));
    }

    @Test
    public void testCheckXWinRowTrue() {
        char turn = 'X';
        char[][] board = {{'X','X','X'},{'X','O','O'},{'O','-','-'}};
        assertEquals(true,OXProgram.Win(board, turn));
    }
    
    @Test
    public void test2CheckXWinRowTrue() {
        char turn = 'X';
        char[][] board = {{'-','O','-'},{'X','X','X'},{'-','-','O'}};
        assertEquals(true,OXProgram.Win(board, turn));
    }

    @Test
    public void test3CheckXWinRowTrue() {
        char turn = 'X';
        char[][] board = {{'O','-','-'},{'O','-','-'},{'X','X','X'}};
        assertEquals(true,OXProgram.Win(board, turn));
    }
    
    @Test
    public void testCheckXWinDiagonaTrue() {
        char turn = 'X';
        char[][] board = {{'-','-','X'},{'-','X','-'},{'X','-','-'}};
        assertEquals(true,OXProgram.Win(board, turn));
    }
    
    @Test
    public void test2CheckXWinDiagonaTrue() {
        char turn = 'X';
        char[][] board = {{'X','-','-'},{'-','X','-'},{'-','-','X'}};
        assertEquals(true,OXProgram.Win(board, turn));
    }

    @Test
    public void testCheckXLoseDiagonaFalse() {
        char turn = 'X';
        char[][] board = {{'-','-','X'},{'-','O','-'},{'X','-','-'}};
        assertEquals(false,OXProgram.Win(board, turn));
    }

    @Test
    public void test2CheckXLoseDiagonaFalse() {
        char turn = 'X';
        char[][] board = {{'X','-','-'},{'-','X','-'},{'-','-','O'}};
        assertEquals(false,OXProgram.Win(board, turn));
    }

    @Test
    public void testCheckOWinColTrue() {
        char turn = 'X';
        char[][] board = {{'X','-','-'},{'X','O','-'},{'X','O','-'}};
        assertEquals(true,OXProgram.Win(board, turn));
    }
     @Test
    public void test2CheckOWinColTrue() {
        char turn = 'X';
        char[][] board = {{'O','X','-'},{'-','X','O'},{'-','X','-'}};
        assertEquals(true,OXProgram.Win(board, turn));
    }
     @Test
    public void test3CheckOWinColTrue() {
        char turn = 'X';
        char[][] board = {{'-','-','X'},{'-','O','X'},{'-','O','X'}};
        assertEquals(true,OXProgram.Win(board, turn));
    }

}
