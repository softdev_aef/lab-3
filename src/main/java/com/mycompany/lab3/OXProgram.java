/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab3;

/**
 *
 * @author fkais
 */
class OXProgram {
    static boolean Win(char[][] board,char turn){
        
        return RowWin(board, turn) || ColWin(board, turn) || DiagonaWin(board, turn) ;
    }
    
    static boolean RowWin(char[][] board,char turn){
        for (int row = 0; row < 3; row++) {
            if (board[row][0] == turn && board[row][1] == turn && board[row][2] == turn) {
                return true;
            }
        }
        return false;
    }
    
    static boolean ColWin(char[][] board,char turn){
        for (int col = 0; col < 3; col++) {
            if (board[0][col] == turn && board[1][col] == turn && board[2][col] == turn) {
                return true;
            }
        }
        return  false;
    }

    static boolean DiagonaWin(char[][] board, char turn) {
        if (board[0][0] == turn && board[1][1] == turn && board[2][2] == turn) {
            return true;
        }
        
        if (board[0][2] == turn && board[1][1] == turn && board[2][0] == turn) {
            return true;
        }
        
        return false;
    }

}
